package com.classpath.inventorymicroservice.service;

import com.classpath.inventorymicroservice.event.OrderEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class OrderService {

    @StreamListener(Sink.INPUT)
    public void processOrder(OrderEvent orderEvent){
      log.info("Processing the order event :: {}", orderEvent);
    }
}
