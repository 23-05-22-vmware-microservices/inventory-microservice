package com.classpath.inventorymicroservice.event;

import com.classpath.inventorymicroservice.model.Order;
import com.classpath.inventorymicroservice.model.OrderStatus;
import lombok.*;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
public final class OrderEvent {
    private Order order;
    private LocalDateTime eventTime;
    private OrderStatus orderStatus;
}

