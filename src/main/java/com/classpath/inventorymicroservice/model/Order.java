package com.classpath.inventorymicroservice.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Order {

    private Long orderId;

    private String customerName;

    private String customerEmail;

    private double price;

}
