package com.classpath.inventorymicroservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/api/inventory")
public class InventoryRestController {

    private static int counter  = 1000;

    @PostMapping
    public int updateCounter(){
        --counter;
        log.info("Inside the update counter method :: " + counter);
        return counter;
    }
    @GetMapping
    public int counter(){
        return counter;
    }
}
